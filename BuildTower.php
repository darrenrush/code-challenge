/*

Build Tower

Build Tower by the following given argument:
number of floors (integer and always greater than 0).

Tower block is represented as *

Python: return a list;
JavaScript: returns an Array;
C#: returns a string[];
PHP: returns an array;
C++: returns a vector<string>;
Haskell: returns a [String];

for example, a tower of 3 floors looks like below

[
  '  *  ', 
  ' *** ', 
  '*****'
]
and a tower of 6 floors looks like below

[
  '     *     ', 
  '    ***    ', 
  '   *****   ', 
  '  *******  ', 
  ' ********* ', 
  '***********'
]

*/

function tower_builder(int $n): array {
  $tower = array();
  $count = $blocks = 1;
  while ($count != $n + 1) {
  $tower[] = str_pad(str_repeat('*',$blocks), $n * 2 - 1, " ", STR_PAD_BOTH);
  $blocks = $blocks + 2;
  $count++;
  }
  return $tower;
}

//tower_builder(6);

//returns
/*
Array
(
    [0] =>      *     
    [1] =>     ***    
    [2] =>    *****   
    [3] =>   *******  
    [4] =>  ********* 
    [5] => ***********
)
*/