<?php //Two-to-One.php

/*

Take 2 strings s1 and s2 including only letters from ato z. Return a new sorted string, the longest possible, containing distinct letters,

each taken only once - coming from s1 or s2. #Examples: ``` a = "xyaabbbccccdefww" b = "xxxxyyyyabklmopq" longest(a, b) -> "abcdefklmopqwxy"
a = "abcdefghijklmnopqrstuvwxyz" longest(a, a) -> "abcdefghijklmnopqrstuvwxyz" ```

*/

function longest($a, $b) {
    $aa = array_unique(str_split($a.$b));
    asort($aa);
    return implode('',$aa);
}

// longest("aretheyhere", "yestheyarehere"); returns aehrsty